﻿using UnityEngine;
using UnityEngine.Events;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

using System.Linq;

public class SceneController : MonoBehaviour
{
    public string Secret = "unicorn";
    public string TestQRCode;

    public GameObject Main;
    public GameObject Start;
    public Transform Content;
    public SortType SortOrder;
    public ClueView CluePrefab;
    public Clue[] Clues;
    public UnityEvent OnCloseCamera;

    public enum SortType { Forward, Reverse, Random }

    private bool Locked
    {
        get => PlayerPrefs.GetInt("locked", 1) > 0;
        set => PlayerPrefs.SetInt("locked", value ? 1 : 0);
    }

    private void Awake()
    {
        if (PlayerPrefs.GetString("version") != Application.version)
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetString("version", Application.version);
        }

        Main.SetActive(!Locked);
        Start.SetActive(Locked);

        CodeReader.OnCodeFinished += (code) =>
        {
            Debug.Log(code);

            if (code == TestQRCode)
            {
                Notification.Show("Testing OK");

                return;
            }

            var clue = CurrentClue();

            if (clue != null && clue.Code == code)
            {
                if (clue.state == Clue.State.unlocked) clue = UnlockNextClue();
                else Notification.Show("Answer the question to unlock next clue");

                if (clue != null)
                {
                    Navigate(clue);
                    OnCloseCamera.Invoke();
                }
                else Notification.Show("You've got the TREASURE!!!");
            }
            else Notification.Show((clue != null && clue.state == Clue.State.pending) ? "Answer the question to unlock next clue" : "This is not the next clue...");
        };

        Refresh();
    }

    private void Navigate(Clue clue)
    {
        if (clue == null) return;

        var rect = Content.GetComponent<RectTransform>();
        var position = rect.anchoredPosition;

        position.y = -clue.view.GetComponent<RectTransform>().anchoredPosition.y;
        rect.anchoredPosition = position;
    }

    public void Reset()
    {
        PlayerPrefs.DeleteAll();

        Refresh();
    }

    private Clue CurrentClue() =>
        Clues.Where(clue => clue.state != Clue.State.locked).OrderBy(clue => clue.order).LastOrDefault();

    private Clue UnlockNextClue()
    {
        var next = Clues.Where(clue => clue.state == Clue.State.locked).OrderBy(clue => clue.order).FirstOrDefault();

        if (next != null)
        {
            next.state = string.IsNullOrEmpty(next.Question) ? Clue.State.unlocked : Clue.State.pending;
            next.Refresh();
        }

        return next;
    }

    private void Initialize()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            Permission.RequestUserPermission(Permission.Camera);
#endif

        if (Clues.All(clue => clue.state == Clue.State.locked))
        {
            var last = Clues.LastOrDefault();
            var index = 0;

            switch (SortOrder)
            {
                case SortType.Forward:
                    foreach (var clue in Clues) clue.order = index++;
                    break;
                case SortType.Reverse:
                    foreach (var clue in Clues.Reverse()) clue.order = clue == last ? Clues.Length : index++;
                    break;
                case SortType.Random:
                    foreach (var clue in Clues.OrderBy(c => c == last ? 2f : Random.value)) clue.order = index++;
                    break;
            }

            UnlockNextClue();
        }
    }

    public void CloseCamera() =>
        OnCloseCamera.Invoke();

    private void Refresh()
    {
        Initialize();

        foreach (Transform child in Content) Destroy(child.gameObject);

        foreach (var clue in Clues.OrderBy(c => c.order))
        {
            clue.view = Instantiate(CluePrefab, Content);
            clue.showCode = SortOrder is not SortType.Random;

            clue.view.Answer.onEndEdit.AddListener(answer =>
            {
                if (!string.IsNullOrEmpty(answer) && !clue.Validate(answer)) Notification.Show("Incorrect answer :(");
            });
            clue.Refresh();
        }
    }

    public void CheckSecret(string value)
    {
        Locked = string.Compare(value, Secret, true) != 0;

        Main.SetActive(!Locked);
        Start.SetActive(Locked);

        if (Locked)
            Notification.Show("C'mon, that's not the secret code");
    }
}
