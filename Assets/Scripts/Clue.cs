﻿using UnityEngine;

[CreateAssetMenu]
public class Clue : ScriptableObject
{
    public enum State { locked, pending, unlocked }

    public Sprite Image;
    public string Code;
    public string Question;
    public string Answer;
    public bool ExactMatch;

    public ClueView view { get; set; }
    public bool showCode { get; set; }

    public State state
    {
        get { return (State)PlayerPrefs.GetInt(Code + ":state"); }
        set { PlayerPrefs.SetInt(Code + ":state", (int)value); }
    }

    public int order
    {
        get { return PlayerPrefs.GetInt(Code + ":order"); }
        set { PlayerPrefs.SetInt(Code + ":order", value); }
    }

    public void SetState(State state)
    {
        this.state = state;

        Refresh();
    }

    public bool Validate(string answer)
    {
        var valid = state == State.pending &&
           ((!ExactMatch && answer.ToLower().Contains(Answer.ToLower())) ||
            (ExactMatch && answer.Equals(Answer, System.StringComparison.OrdinalIgnoreCase)));

        if (valid)
        {
            state = State.unlocked;
            Refresh();
        }

        return valid;
    }

    public void Refresh()
    {
        if (view != null) view.Refresh(this);
    }
}
