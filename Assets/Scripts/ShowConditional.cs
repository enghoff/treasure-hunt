﻿using UnityEngine;

using System.Linq;

public class ShowConditional : MonoBehaviour
{
    public RuntimePlatform[] Platforms;

    private void Awake()
    {
        gameObject.SetActive(Platforms.Contains(Application.platform));
    }
}
