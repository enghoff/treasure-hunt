﻿using UnityEngine;

public class Answer : MonoBehaviour
{
    public Sprite Sprite;
    public string CorrectAnswer;
    public bool PerfectMatch;
}
