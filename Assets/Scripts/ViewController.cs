﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Text;
using System.Linq;

public class ViewController : MonoBehaviour
{
    public Image Background;
    public InputField InputField;
    public Text CountDown;
    public Sprite DefaultBackground;
    public AudioSource Success;
    public AudioSource Fail;

    private void Awake()
    {
        Reset();
    }

    private void Update()
    {
        var duration = new DateTime(DateTime.Now.Year + 1, 1, 1, 0, 0, 0) - DateTime.Now;

        CountDown.text = Mathf.FloorToInt((float)duration.TotalHours).ToString("0") + ":" + duration.Minutes.ToString("00") + ":" + duration.Seconds.ToString("00");
    }

    public void Reset()
    {
        Background.sprite = DefaultBackground;
        InputField.text = "";
        InputField.Select();
    }

    private static string Condition(string str)
    {
        return RemoveSpecialCharacters(str).ToLower();
    }

    private static string RemoveSpecialCharacters(string str)
    {
        var stringBuilder = new StringBuilder();

        foreach (var c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) stringBuilder.Append(c);
        }
        return stringBuilder.ToString();
    }

    public void CheckAnswer()
    {
        if (string.IsNullOrEmpty(InputField.text)) return;

        var answer = Condition(InputField.text);
        var match = GetComponents<Answer>().FirstOrDefault(a => a.CorrectAnswer == answer || (!a.PerfectMatch && answer.Contains(Condition(a.CorrectAnswer))));

        if (match != null)
        {
            Background.sprite = match.Sprite;
            Success.Play();
        }
        else
        {
            Background.sprite = DefaultBackground;
            Fail.Play();
        }
    }
}
