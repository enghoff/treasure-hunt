﻿using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    public Text Message;
    public Animator Animator;

    private static Notification _instance;

    private void Awake()
    {
        _instance = this;
    }

    public static void Show(string message)
    {
        _instance.Message.text = message;
        _instance.Animator.SetTrigger("Show");
    }
}
