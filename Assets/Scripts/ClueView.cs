﻿using UnityEngine;
using UnityEngine.UI;

public class ClueView : MonoBehaviour
{
    public GameObject Locked;
    public GameObject Pending;
    public Text Question;
    public InputField Answer;
    public Image Image;
    public Text Code;

    public void Refresh(Clue clue)
    {
        Question.text = clue.Question;

        switch (clue.state)
        {
            case Clue.State.locked:
                Locked.SetActive(true);
                Pending.SetActive(false);
                Code.text = string.Empty;
                break;
            case Clue.State.pending:
                Locked.SetActive(false);
                Pending.SetActive(true);
                Code.text = string.Empty;
                break;
            case Clue.State.unlocked:
                Locked.SetActive(false);
                Pending.SetActive(false);
                Image.sprite = clue.Image;
                Image.color = Color.white;
                Code.text = clue.showCode ? clue.Code : string.Empty;
                break;
        }
    }
}
